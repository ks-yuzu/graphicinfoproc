#include <array>
#include <vector>
#include <list>
#include <random>

#include "input.h"
#include "fps.h"

#include "define.h"
#include "my-mat.h"
#include "window.h"
#include "trackbar.h"
#include "camera.h"
#include "image-io.h"
#include "movie-io.h"

#include "debug-window/disp_window.h"
#include "debug-window/log_window.h"

#include "template-match.h"

#include "panorama.h"

void ShowNote(std::array<Window, 3> &);


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	cv::initModule_nonfree();

	InputMgr& key = InputMgr::GetInstance();		//singleton
	FpsControl& fps = FpsControl::GetInstance();	//singleton
	std::array<Window, 3> windows;
	LogWindow::Init();

	Camera camera(0);
	camera.CheckOpened();

	// mouse interface
	iMouseInput *mouse0 = &windows[0]; //window0に対応する入力

	// template matching
	TemplateMatch tmatch(mouse0);

	cv::Mat testImg;
	ImageIO::LoadImg(testImg, "img.jpg");
	Panorama panorama( cv::Mat(testImg, cv::Rect(0, 500, 500, 800)) );

	int count = 0;
	std::mt19937_64 mt;

	while( fps.SetStartTime(), Window::ProcessMessage() && key.Update() && camera.Update() )
	{
		static const cv::Point mousePos = windows[0].MousePos();
		static cv::Mat res;


		//=========== 実処理部 ===========
//		const MyMat input = camera().clone();

		// 手振れの再現として、上下の揺れをランダムに生成
		std::uniform_int_distribution<int> dist(-50, 50);
		int rand = dist(mt);

		// 時間経過で領域を移動させる
		cv::Point lts(  0 + count,  500 + rand);
		cv::Point rbs(510 + count, 1300 + rand);
		const cv::Mat input(testImg, cv::Rect(lts, rbs));

		cv::Mat input_copy = input.clone();

		// パノラマ合成処理は全てこの関数の中
		if( count++ % 60 == 59 )
			panorama.Update(input.clone());
		const cv::Mat& pnrmImg = panorama.GetPanoramaImage();


		//=========== 描画部 ===========
		windows[0](input_copy, "camera");
		windows[1](pnrmImg, "panorama");
//		windows[2](res, "matching");

		//=========== 動画像保存 ===========
		ImageIO::SaveImg( input );
//		movWriter.Rec( input );

		//=========== ループ後処理 ===========
		ShowNote(windows);

		mouse0->UpdateMouseFlag();

		DispWindow::AllWindowFlip();
		fps.Update();
		fps.Wait();
	}

	return 0;
}


void ShowNote(std::array<Window, 3>& windows)
{
	DispWindow& disp = *DispWindow::GetDispWindow(0);

	disp << Command::endline;
	disp << "opencv version : " << CV_VERSION << Command::endline;

	disp << Command::endline;
	disp << "mouse (x, y) = (" << windows[0].MousePos().x << " ," << windows[0].MousePos().y << ")" << Command::endline; 
	disp << "mouse Click : " << windows[0].ClickFlag() << Command::endline;
	disp << "mouse NowDown : " << windows[0].NowDownFlag() << Command::endline;
	disp << "mouse NowUp : " << windows[0].NowUpFlag() << Command::endline;

	disp << Command::endline;
	disp << "s + j ... save as jpg" << Command::endline;
	disp << "s + p ... save as png" << Command::endline;
	disp << "s + b ... save as bmp" << Command::endline;

	disp << Command::endline;
	disp << "v + b ... video rec start" << Command::endline;
	disp << "v + e ... video rec stop"  << Command::endline;

	disp << Command::endline;
	disp << "  u   ... update background" << Command::endline;

}



