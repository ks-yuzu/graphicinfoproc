#pragma once

#include "opencv.h"


class MyMat : public cv::Mat
{
public:
	MyMat(){}
	MyMat(std::string filename)	{ LoadImg(filename); }
	MyMat(cv::Size size, int type) : cv::Mat(size, type) {}
	MyMat(cv::Mat& mat)
	{
		*(cv::Mat *) this = mat.clone();
	}

	virtual ~MyMat() = default;

	MyMat& operator=(cv::Mat& mat)
	{
		*(cv::Mat *) this = mat.clone();
		return *this;
	}

	void ToGray();
	void ToGray(const cv::Mat&);
	cv::Mat MakeGray() const;
	void ToHsv();
	void ToHsv(const cv::Mat&);
	std::vector<cv::Mat> GetHsvMats() const;
	std::vector<cv::Mat> GetBgrMats() const;
	void ToMask(const cv::Mat&, const cv::Scalar&, const cv::Scalar&);
	void Mask(const cv::Mat&, const cv::Scalar&, const cv::Scalar&);
	void Mask(const cv::Mat&, const cv::Mat&);

	void Closing(int num);
	void Opening(int num);

	bool SaveImg();
	bool SaveImg(std::string name);
	bool LoadImg(std::string& name);
};

