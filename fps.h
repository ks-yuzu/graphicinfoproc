#pragma once
#include <string>
#include <Windows.h>

class FpsControl
{
	public:
	//LIFECYCLE
		FpsControl(FpsControl&);


	//ACCESS
		static FpsControl& GetInstance();

	//OPERRATIONS
		void SetStartTime();
		void Update();
		void Wait();
		int GetFps(){return (int)fps;};

	private:
	//CONSTANTS
		static const int MAX_COUNT = 10;
		static const int STANDERD_FPS = 60;

	//LIFECYCLE
		FpsControl();
		virtual ~FpsControl() = default;

	//OPERATIONS
		void CalcFps();
		void Draw();

	//VARIABLES
		DWORD startTime, startCountingTime;
		int count;
		double fps;
		std::string strFps;
};

