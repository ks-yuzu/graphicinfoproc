#pragma once

#include "opencv.h"

// マウス入力取得インターフェイス
class iMouseInput
{
	public:
		virtual const cv::Point& MousePos() const = 0;
		virtual bool ClickFlag() const            = 0;
		virtual bool NowDownFlag() const          = 0;
		virtual bool NowUpFlag() const            = 0;
		virtual void UpdateMouseFlag()            = 0;
};