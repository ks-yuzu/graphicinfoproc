#pragma once
#include "opencv.h"
#include "input.h"
#include "debug-window/log_window.h"


class ImageIO
{
	public:
		// ==================   IMG INPUT    ====================
		// IOの対称性のためのリダイレクト
		static bool LoadImg(cv::Mat& image, const std::string& name)
		{
			return ReadImage(image, name);
		}


		// ==================   IMG OUTPUT    ====================
		// 形式ごとの保存
		static void WriteJpg(const cv::Mat& image, std::string name)
		{
			if( name.substr(name.length() - 4, 4) != ".jpg" )	name += ".jpg";
			WriteImage(image, name);
		}

		static void WritePng(const cv::Mat& image, std::string name)
		{
			if( name.substr(name.length() - 4, 4) != ".png" )	name += ".png";
			WriteImage(image, name);
		}

		static void WriteBmp(const cv::Mat& image, std::string name)
		{
			if( name.substr(name.length() - 4, 4) != ".bmp" )	name += ".bmp";
			WriteImage(image, name);
		}

		static bool SaveImg(const cv::Mat& image, const std::string& name = "NoTitle")
		{
			static InputMgr& input = InputMgr::GetInstance();

			if( input('S') && input('J') == 1 )
				WriteJpg(image, name);

			if( input('S') && input('P') == 1 )
				WritePng(image, name);

			if( input('S') && input('B') == 1 )
				WriteBmp(image, name);

			return true;
		}



	private:
		// ==================   IMG INPUT    ====================
		static bool ReadImage(cv::Mat& image, const std::string& name)
		{
			image = cv::imread(name);
			logwnd << "[ imread ]  " << name << (image.data != NULL ? "  ... success" : "  ... failure") << Command::endline;

			return !image.empty() ? true : false;
		}


		// ==================   IMG OUTPUT    ====================
		static bool WriteImage(const cv::Mat& image, const std::string& name)
		{
			bool fSuccess = cv::imwrite(name, image);
			logwnd << "[ imwrite ]  " << name << (fSuccess ? "  ... success" : "  ... failure") << Command::endline;

			return fSuccess ? true : false;
		}



};