#include "image-io.h"
#include "my-mat.h"
#include "define.h"

// 濃淡画像に変換
void MyMat::ToGray()
{
	ToGray(*this);
}

void MyMat::ToGray(const cv::Mat& input)
{
	cv::cvtColor(input, *this, CV_BGR2GRAY);
}

cv::Mat MyMat::MakeGray() const
{
	cv::Mat tmp;
	cv::cvtColor(*this, tmp, CV_BGR2GRAY);
	return tmp.clone();
}


// HSV成分に変換
void MyMat::ToHsv()
{
	ToHsv(*this);
}

void MyMat::ToHsv(const cv::Mat& input)
{
	cv::cvtColor(input, *this, CV_BGR2HSV);
}


std::vector<cv::Mat> MyMat::GetHsvMats() const
{
	cv::Mat hsv;
	std::vector<cv::Mat> panels(3);

	cv::cvtColor(*this, hsv, CV_BGR2HSV);
	cv::split(hsv, panels);

	return panels;
}

std::vector<cv::Mat> MyMat::GetBgrMats() const
{
	std::vector<cv::Mat> panels;
	cv::split(*this, panels);

	return panels;
}


//しきい値を用いたマスク画像の作成
void MyMat::ToMask(const cv::Mat& input, const cv::Scalar& low, const cv::Scalar& high)
{
	MyMat hsv;
	hsv.ToHsv(input);
	cv::inRange(hsv, low, high, *this);
}

// マスク処理
void MyMat::Mask(const cv::Mat& input, const cv::Scalar& low, const cv::Scalar& high)
{
	MyMat mask;
	mask.ToMask(input, low, high);
	input.copyTo(*this, mask);
}

void MyMat::Mask(const cv::Mat& input, const cv::Mat& mask)
{
	input.copyTo(*this, mask);
}


// モルフォジー処理
void MyMat::Closing(int num)
{
	cv::dilate(*this, *this, cv::Mat(), cv::Point(-1, -1), num);
	cv::erode(*this, *this, cv::Mat(), cv::Point(-1, -1), num);
}

void MyMat::Opening(int num)
{
	cv::erode(*this, *this, cv::Mat(), cv::Point(-1, -1), num);
	cv::dilate(*this, *this, cv::Mat(), cv::Point(-1, -1), num);
}



//cv::Mat用の入出力クラス（image-io.h）の処理へリダイレクト
bool MyMat::SaveImg()
{
	return ImageIO::SaveImg( *(cv::Mat *)this );
}

bool MyMat::SaveImg(std::string name)
{
	return ImageIO::SaveImg( *(cv::Mat *)this, name );
}

bool MyMat::LoadImg(std::string& name)
{
	return ImageIO::LoadImg(*(cv::Mat *)this, name);
}