#include <cassert>
#include "log_window.h"


//using namespace Log;

LogWindow logwnd;

void LogWindow::Init()
{
	//ログウィンドウの設定とタイトル
	logwnd.OutputWindow::Init();
	logwnd << "Log Window" << Command::endline << Command::endline;
}

LogWindow::LogWindow()
:fDispTime(false)
{}


void LogWindow::InitStartupInfo(STARTUPINFO& si)
{
	const WORD colors[] = {
		FOREGROUND_RED   | FOREGROUND_INTENSITY,						//DUMMY
		FOREGROUND_GREEN | FOREGROUND_INTENSITY,						//Green
		FOREGROUND_BLUE  | FOREGROUND_RED   |  FOREGROUND_INTENSITY,	//Purple
		FOREGROUND_BLUE  | FOREGROUND_GREEN |  FOREGROUND_INTENSITY,	//Cyan
		FOREGROUND_GREEN | FOREGROUND_RED   |  FOREGROUND_INTENSITY,	//Yellow
		FOREGROUND_BLUE  | FOREGROUND_INTENSITY,						//Blue
		FOREGROUND_RED   | FOREGROUND_INTENSITY,						//Red
	};

	titleName = "LogWindow";
	commandLine = logConsole;

	ZeroMemory( &si, sizeof(STARTUPINFO) );

	si.cb				= sizeof(STARTUPINFO);
	si.dwFlags			= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW | STARTF_USEPOSITION | STARTF_USEFILLATTRIBUTE;
	si.wShowWindow		= SW_SHOWNOACTIVATE;
	si.lpTitle			= const_cast<LPSTR>( titleName.c_str() );
	si.dwX				= 10;
	si.dwY				= 20;
	si.dwFillAttribute	= colors[5];
	si.hStdInput		= readPipe;
	si.hStdOutput		= GetStdHandle(STD_OUTPUT_HANDLE);
	si.hStdError		= GetStdHandle(STD_ERROR_HANDLE);

	// 確保した標準出力のチェック
	assert( si.hStdOutput != INVALID_HANDLE_VALUE );
	assert( si.hStdError != INVALID_HANDLE_VALUE );
}




void LogWindow::PrintTime()
{
	using namespace std::chrono;

	auto tp = system_clock::now();
	std::time_t now = system_clock::to_time_t(tp);
	buf << std::put_time(std::localtime(&now), "%X") << "  ";

	fDispTime = false;
}


