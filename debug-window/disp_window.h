#pragma once

#include "output_window.h"
#include "output_data.h"


class DispWindow : public OutputWindow
{
public:
	virtual ~DispWindow() = default;

	//OPERATORS
	template<class T>
	DispWindow& operator<<(T);

	//OPERATIONS
	static void AllWindowFlip();

	//ACCESS
	static std::shared_ptr<DispWindow> GetDispWindow(int idx);

private:
	static std::vector<std::shared_ptr<DispWindow> > outputs;
	int windowNum;

	DispWindow(int);

	void InitStartupInfo(STARTUPINFO&) override;

};


template<class T>
inline DispWindow& DispWindow::operator<<(T val)
{
	std::stringstream buf;
	buf << val;
	Print( buf.str() );

	return *this;
}


inline std::shared_ptr<DispWindow> DispWindow::GetDispWindow(int idx)
{
	if( !outputs[idx] )
	{
		outputs[idx] = std::shared_ptr<DispWindow>(new DispWindow(idx));
		outputs[idx]->Init();
	}

	return outputs[idx];
}


inline void DispWindow::AllWindowFlip()
{
	for each (std::shared_ptr<DispWindow> ow in outputs)
	{
		if( !ow ) continue;
		*ow << Command::flip;
	}
}