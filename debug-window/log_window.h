#pragma once
#pragma warning(disable : 4996)

#include <chrono>
#include <iomanip>

#include "output_window.h"
#include "output_data.h"

//namespace Log
//{

	class LogWindow : public OutputWindow
	{
		public:
			LogWindow();
			virtual ~LogWindow() = default;
			template<class T> LogWindow& operator<<(T val);

			static void Init();

		private:
			bool fDispTime;
			std::stringstream buf;

			void InitStartupInfo(STARTUPINFO&) override;

			void PrintTime();
			template<class T> void PrintData(T);
	};


	template<class T>
	inline void LogWindow::PrintData(T val)
	{
		buf << val;
	}


	extern LogWindow logwnd;
//	void LogWndInit();




	template<typename T>
	inline LogWindow& LogWindow::operator<<(T val)
	{
		if( fDispTime )
			PrintTime();

		PrintData(val);

		return *this;
	}

	template<>
	inline LogWindow& LogWindow::operator<< <std::string>(std::string val)
	{
		if( fDispTime && val != Command::endline )
			PrintTime();

		PrintData(val);

		if( val == Command::endline )
		{
			Print(buf.str());
			buf.str("");
			buf.clear(std::stringstream::goodbit);
			fDispTime = true;
		}

		return *this;
	}

//}// namespace Log
