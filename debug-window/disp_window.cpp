#include <cassert>
#include "disp_window.h"


std::vector<std::shared_ptr<DispWindow> > DispWindow::outputs(6);

DispWindow::DispWindow(int wn)
:windowNum(wn+1)
{}

void DispWindow::InitStartupInfo(STARTUPINFO& si)
{
	const WORD colors[] = {
		FOREGROUND_RED   | FOREGROUND_INTENSITY,						//DUMMY
		FOREGROUND_GREEN | FOREGROUND_INTENSITY,						//Green
		FOREGROUND_BLUE  | FOREGROUND_RED   |  FOREGROUND_INTENSITY,	//Purple
		FOREGROUND_BLUE  | FOREGROUND_GREEN |  FOREGROUND_INTENSITY,	//Cyan
		FOREGROUND_GREEN | FOREGROUND_RED   |  FOREGROUND_INTENSITY,	//Yellow
		FOREGROUND_BLUE  | FOREGROUND_INTENSITY,						//Blue
		FOREGROUND_RED   | FOREGROUND_INTENSITY,						//Red
	};

	titleName = "OutputWindow No." + std::to_string(windowNum);
	commandLine = dispConsole;

	ZeroMemory( &si, sizeof(STARTUPINFO) );

	si.cb				= sizeof(STARTUPINFO);
	si.dwFlags			= STARTF_USESTDHANDLES | STARTF_USESHOWWINDOW | STARTF_USEPOSITION | STARTF_USEFILLATTRIBUTE;
	si.wShowWindow		= SW_SHOWNOACTIVATE;
	si.lpTitle			= const_cast<LPSTR>( titleName.c_str() );
	si.dwX				= 10 + 650 * (windowNum / 2);
	si.dwY				= 20 + 494 * (windowNum % 2);
	si.dwFillAttribute	= colors[windowNum];
	si.hStdInput		= readPipe;
	si.hStdOutput		= GetStdHandle(STD_OUTPUT_HANDLE);
	si.hStdError		= GetStdHandle(STD_ERROR_HANDLE);

	// 確保した標準出力のチェック
	assert( si.hStdOutput != INVALID_HANDLE_VALUE );
	assert( si.hStdError != INVALID_HANDLE_VALUE );
}