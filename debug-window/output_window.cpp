#include <iostream>
#include <string>
#include <cassert>
#include <process.h>
#include "output_window.h"
#include "output_data.h"


const std::string OutputWindow::dispConsole = "OutputWindow.exe /D";
const std::string OutputWindow::logConsole  = "OutputWindow.exe /L";


OutputWindow::OutputWindow()
:hParenProcess( GetCurrentProcess() ),
 hChildProcess(NULL),
 readPipe(nullptr),
 writePipe(nullptr)
{}


OutputWindow::~OutputWindow()
{
//	GenerateConsoleCtrlEvent(CTRL_BREAK_EVENT, procGroupId);
	TerminateProcess(hChildProcess, EXIT_SUCCESS);
	CloseHandle(writePipe);
	CloseHandle(hChildProcess);
}


void OutputWindow::Init()
{
	HANDLE tmpReadPipe;

	// パイプを作成（両ハンドルとも子プロセスへ継承不可）
	assert( CreatePipe(&tmpReadPipe, &writePipe, NULL, 0) );

	// 読込ハンドルを複製（子プロセスへ継承可能な権限の読込ハンドルを作成）
	assert( DuplicateHandle( hParenProcess, tmpReadPipe, hParenProcess, &readPipe, 0, TRUE, DUPLICATE_SAME_ACCESS) );

	// 複製元のハンドルは使わないのでクローズ
	assert( CloseHandle(tmpReadPipe) );

	InitStartupInfo(si);

	StartProcess();
}


bool OutputWindow::StartProcess()
{
	hProcThread = (HANDLE)_beginthreadex(NULL, 0, LaunchThread, this, 0, NULL);
//	hProcThread = (HANDLE)_beginthreadex(NULL, 0, this->TProcess, this, 0, NULL);

	if( hProcThread != nullptr )
		CloseHandle( hProcThread );

	return true;
}


void OutputWindow::Print(std::string str)
{
	DWORD dummy;
	str += '|';

	WriteFile(writePipe, str.c_str(), strlen(str.c_str()), &dummy, NULL);
}



//=======================================
//           子プロセス側
//=======================================

// staticでないメンバ関数をマルチスレッドにするために、この関数をかませる
unsigned int __stdcall OutputWindow::LaunchThread(void *ptr)
{
	return reinterpret_cast<OutputWindow*>(ptr)->TProcess();
}


unsigned int __stdcall OutputWindow::TProcess()
{
	ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
	assert( CreateProcess(NULL, (LPSTR)commandLine.c_str(), NULL, NULL, TRUE, CREATE_NEW_CONSOLE | CREATE_NEW_PROCESS_GROUP, NULL, NULL, &si, &pi) );
//	procGroupId = pi.dwProcessId;

	// Set child process handle to cause threads to exit.
	hChildProcess = pi.hProcess;

	// Close any unnecessary handles.
	CloseHandle(pi.hThread);

	// Close pipe handles (do not continue to modify the parent)
	// Need to make sure that no handles to the write end of the output pipe are maintained in this process
	// or else the pipe will not close when the child process exits
	// and the ReadFile will hang.
	CloseHandle(readPipe);
	readPipe = nullptr;

	return 0;

}