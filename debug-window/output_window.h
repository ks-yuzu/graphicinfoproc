#pragma once

#include <string>
#include <sstream>
#include <memory>
#include <vector>
#include <Windows.h>

#include "output_data.h"

class OutputWindow : public Command
{
public:
	//LIFECYCLE
	virtual ~OutputWindow();

	//OPRATOR
	template<class T>
	OutputWindow& operator<<(T val);

	//OPERATIONS
	void Init();
	void Print(std::string);


protected:
	static const std::string dispConsole;
	static const std::string logConsole;

	//VARIABLES
	std::string commandLine;
	std::string titleName;
	HANDLE readPipe;
	DWORD procGroupId;

	OutputWindow();

private:
	STARTUPINFO si;
	PROCESS_INFORMATION pi;
	HANDLE hProcThread;
	HANDLE hParenProcess;							// 親プロセス(カレントプロセス)のハンドル
	HANDLE hChildProcess;							// 子プロセスのハンドル
	HANDLE writePipe;

	//OPERATIONS
	virtual void InitStartupInfo(STARTUPINFO&) = 0;
	bool StartProcess();
	static unsigned int __stdcall LaunchThread(void *);
	unsigned int __stdcall TProcess();

};


template<class T>
inline OutputWindow& OutputWindow::operator<<(T val)
{
	std::stringstream buf;
	buf << val << '|';
	Print( buf.str() );

	return *this;
}