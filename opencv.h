#pragma once

//#define KSC_PC
#define MY_PC

#ifdef KSC_PC
	#include "C:\Program Files (x86)\opencv\build\include\opencv2\opencv.hpp"
	#pragma comment(lib,"C:\\Program Files (x86)\\opencv\\build\\x86\\vc10\\lib\\opencv_core231d.lib")
	#pragma comment(lib,"C:\\Program Files (x86)\\opencv\\build\\x86\\vc10\\lib\\opencv_imgproc231d.lib")
	#pragma comment(lib,"C:\\Program Files (x86)\\opencv\\build\\x86\\vc10\\lib\\opencv_highgui231d.lib")
	#pragma comment(lib,"C:\\Program Files (x86)\\opencv\\build\\x86\\vc10\\lib\\opencv_legacy231d.lib")
	#pragma comment(lib,"C:\\Program Files (x86)\\opencv\\build\\x86\\vc10\\lib\\opencv_video231d.lib")
#endif

#ifdef MY_PC
	#include "D:\Documents\Visual Studio 2013\Libraries\opencv-2.4.10\build\include\opencv2\opencv.hpp"
	#include "D:\Documents\Visual Studio 2013\Libraries\opencv-2.4.10\build\include\opencv2\legacy\legacy.hpp"
	#include "D:\Documents\Visual Studio 2013\Libraries\opencv-2.4.10\build\include\opencv2\nonfree\nonfree.hpp"  //surfに必要

	#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_core2410d.lib")
	#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_imgproc2410d.lib")
	#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_highgui2410d.lib")
	#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_legacy2410d.lib")
	#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_video2410d.lib")
	#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_nonfree2410d.lib" )  //surfに必要
	#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_features2d2410d.lib" )  //surfに必要
	#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_flann2410d.lib" )  //surfに必要
	#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_calib3d2410d.lib" )  //surfに必要

	//#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_core2410.lib")
	//#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_imgproc2410.lib")
	//#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_highgui2410.lib")
	//#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_legacy2410.lib")
	//#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_video2410.lib")
	//#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_nonfree2410.lib" )  //surfに必要
	//#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_features2d2410.lib" )  //surfに必要
	//#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_flann2410.lib" )  //surfに必要
	//#pragma comment(lib, "D:\\Documents\\Visual Studio 2013\\Libraries\\opencv\\build\\x86\\vc12\\lib\\opencv_calib3d2410.lib" )  //surfに必要

#endif
