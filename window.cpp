#include "window.h"

int Window::cnt = 0;
std::function<std::string(int)> Window::defaultName = [](int cnt){ return ("Window" + std::to_string(cnt)); };


void RedirectMouse(int evnt, int x, int y, int flags, void* param)
{
	auto pw = (Window *)param;
	pw->ProcessMouseEvent(evnt, x, y, flags);
}

Window::Window(std::string name, cv::Size size)
: windowName(name), mousePos(0, 0), fClick(false), fNowDown(false), fNowUp(false)
{
	cv::namedWindow(windowName.c_str(), CV_WINDOW_AUTOSIZE);
	cvMoveWindow(windowName.c_str(), 680 + 600 * (cnt / 2), 20 + 490 * (cnt % 2) );
	cv::resizeWindow(windowName, size.width, size.height);

	cv::setMouseCallback(windowName, RedirectMouse, this);
	cnt++;
}


Window::~Window()
{
	cv::destroyWindow(windowName.c_str());
}

void Window::ProcessMouseEvent(int evnt, int x, int y, int flags)
{
	mousePos = cv::Point(x, y);

	switch( evnt )
	{
		case cv::EVENT_LBUTTONDOWN:
			fClick = true;
			fNowDown = true;
			break;

		case cv::EVENT_LBUTTONUP:
			fClick = false;
			fNowUp = true;
			break;

		default:
			break;
	}
}