#pragma once

#include <string>
#include <functional>
#include <list>
#include "opencv.h"
#include "debug-window\log_window.h"
#include "define.h"
#include "i-mouse.h"


class Window : public iMouseInput
{
	public:
		Window(std::string name = defaultName(cnt), cv::Size = wndSize);
		~Window();

		void operator()(const IplImage*, const std::string& message = "") const;
		void operator()(const cv::Mat&, const std::string& message = "") const;

		const std::string& GetName() const { return windowName; }

		static bool ProcessMessage();
		void ShowImage(const IplImage*, const std::string&) const;
		void ShowImage(const cv::Mat&, const std::string&) const;

		// インターフェイス iMouseInput の実装
		const cv::Point& MousePos() const override { return mousePos; }
		bool ClickFlag()            const override { return fClick;   }
		bool NowDownFlag()          const override { return fNowDown; }
		bool NowUpFlag()            const override { return fNowUp;   }
		void UpdateMouseFlag()            override { fNowDown = fNowUp = false; };

		void ProcessMouseEvent(int evnt, int x, int y, int flags);

	private:
		static int cnt;
		static std::function<std::string(int)> defaultName;

		const std::string windowName;

		cv::Point mousePos;
		bool fClick, fNowDown, fNowUp;
};


inline void Window::operator()(const IplImage *const pImage, const std::string& message) const
{
	ShowImage(pImage, message);
}

inline void Window::operator()(const cv::Mat& image, const std::string& message) const
{
	ShowImage(image, message);
}


inline void Window::ShowImage(const IplImage *const pImage, const std::string& message) const
{
	ShowImage( (cv::Mat)pImage, message );
}

inline void Window::ShowImage(const cv::Mat& image, const std::string& message) const
{
	if( image.empty() )
	{
		logwnd << "image for \"" << windowName << "\" is empty" << Command::endline;
		return;
	}

	cv::Mat copy = image.clone();
	std::string buf = std::to_string(image.size().width) + "*" + std::to_string(image.size().height) + "  " + message;
	cv::putText(copy, buf, cv::Point(10, 30), CV_FONT_HERSHEY_COMPLEX_SMALL, 0.7, cv::Scalar(0, 255, 0)); 

	cv::imshow(windowName, copy);

}

inline bool Window::ProcessMessage()
{
	cv::waitKey(1);
	return true;
}

