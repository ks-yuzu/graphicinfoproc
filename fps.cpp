#include <iostream>
#include <sstream>
#include <iomanip>
#include "fps.h"
#include "opencv.h"
#pragma comment(lib, "winmm.lib")

#include "debug-window/disp_window.h"
#include "debug-window/log_window.h"

FpsControl::FpsControl()
:startTime(timeGetTime()), count(0), fps(0)
{}


FpsControl::FpsControl(FpsControl& aFps)
:startTime(aFps.startTime), count(aFps.count), fps(aFps.fps)
{}


FpsControl& FpsControl::GetInstance()
{
	static FpsControl fps;
	return fps;
}


void FpsControl::SetStartTime()
{
	startTime = timeGetTime();
	Draw();
}

void FpsControl::Update()
{
	count++;

	if( count == MAX_COUNT )
	{
		CalcFps();
		count = 0;
		startCountingTime = timeGetTime();
	}
}


void FpsControl::CalcFps()
{
	int now = timeGetTime();
	fps = 1000. * MAX_COUNT / ( now - startCountingTime );

	std::ostringstream buf;
	buf << "FPS : " << std::fixed << std::setprecision(2) << fps;
	strFps = buf.str();
}


void FpsControl::Draw()
{
	*DispWindow::GetDispWindow(0) << strFps << Command::endline;
}


void FpsControl::Wait()
{
	double waitTime;

	do{
		int passTime = timeGetTime() - startTime;
		waitTime = 1000. / STANDERD_FPS - passTime;

//		Log::log << "passTime : " << passTime << " ms" << Command::endline;		
		Sleep( 1 );
	}while( waitTime >= 1 );
}

