#include "opencv.h"

class Panorama
{
	public:
		Panorama(cv::Mat ini) : panoramaImg(ini) {};

		void Update(cv::Mat& input);
		const cv::Mat& GetPanoramaImage() { return panoramaImg; };

	private:
		// 合成したパノラマ画像
		cv::Mat panoramaImg;

		// SURF関連
		cv::SurfFeatureDetector detector;       // 特徴点抽出インターフェイス
		cv::SurfDescriptorExtractor extractor;  // 特徴記述インターフェイス

		// 特徴点用配列
		std::vector<cv::KeyPoint> keypointsPanorama;
		std::vector<cv::KeyPoint> keypointsInput;

		// featurePointにおける特徴ベクトル
		cv::Mat descriptorPanorama;
		cv::Mat descriptorInput;
};



inline void Panorama::Update(cv::Mat& input_copy)
{
	if( input_copy.empty() ||  panoramaImg.empty() ) return;

	// 正規化2値画像の作成
	cv::Mat panoramaGray, inputGray;
	cv::cvtColor(panoramaImg, panoramaGray, CV_BGR2GRAY);
	cv::cvtColor(input_copy, inputGray, CV_BGR2GRAY);
	cv::normalize(panoramaGray, panoramaGray, 0, 255, cv::NORM_MINMAX);
	cv::normalize(inputGray, inputGray, 0, 255, cv::NORM_MINMAX);

	// 特徴点の位置とスケール抽出
	detector.detect(panoramaGray, keypointsPanorama);
	detector.detect(inputGray, keypointsInput);

	// 上記特徴点における特徴ベクトル（輝度勾配ヒストグラム）抽出
	extractor.compute(panoramaGray, keypointsPanorama, descriptorPanorama);
	extractor.compute(inputGray, keypointsInput, descriptorInput);

	// マッチング処理
	std::vector<cv::DMatch> matchPnrmToIn, matchInToPnrm;  // DMatch : ディスクリプタのマッチングインターフェイス
	cv::BruteForceMatcher<cv::L2<float>> matcher;
//	cv::FlannBasedMatcher matcher;
	matcher.match(descriptorPanorama, descriptorInput, matchPnrmToIn);
	matcher.match(descriptorInput, descriptorPanorama, matchInToPnrm);

	// クロスチェック(ブルートフォースに対する高精度化処理)
	std::vector<cv::DMatch>& match = matchInToPnrm;
	auto min = [](auto a, auto b){ return a < b ? a : b; }; // VC2015 only
	for(size_t i = 0; i < min(matchPnrmToIn.size(), matchInToPnrm.size()); ++i)
	{
		const cv::DMatch& fw = matchInToPnrm[i];
		const cv::DMatch& bw = matchPnrmToIn[fw.trainIdx];
		if( bw.trainIdx == fw.queryIdx ) { match.push_back(fw); }
	}

	// keypointの選別
	//double minDist = 10000, maxDist = 0;
	//for (size_t i = 0; i < match.size(); ++i)
	//{
	//	double dist = match[i].distance;
	//	minDist = minDist < dist ? minDist : dist;
	//	maxDist = maxDist > dist ? maxDist : dist;
	//}

	std::vector<cv::DMatch>& selectedMatch = match;
	//for (size_t i = 0; i < match.size(); ++i)
	//{
	//	if (match[i].distance < 5 * minDist)
	//	{
	//		selectedMatch.push_back(match[i]);
	//	}
	//}

	// ↑　一部の高精度化処理はコメントアウトしている

	// マッチ結果が得られた場合のみ処理
	if( selectedMatch.size() > 10 )
	{

		// 対応を表す変換行列の作成（ホモグラフィ行列）
		std::vector<cv::Point2f> inVec, pnrmVec;
		for(auto dmatch : selectedMatch)
		{
			inVec.push_back(keypointsInput[dmatch.queryIdx].pt);
			pnrmVec.push_back(keypointsPanorama[dmatch.trainIdx].pt);
		}
		cv::Mat translateMat = cv::findHomography(inVec, pnrmVec, cv::RANSAC);


		// ----- パノラマ合成処理 -----
		cv::Mat res;

		// 入力画像の移動
		cv::warpPerspective( input_copy, res, translateMat, cv::Size( panoramaImg.size().width + 60, panoramaImg.size().height) );

		// バッファ画像の上書き
		for (int y = 0; y < panoramaImg.rows; y++)
		{
			for (int x = 0; x < panoramaImg.cols; x++)
			{
				res.at<cv::Vec3b>(y, x) = panoramaImg.at<cv::Vec3b>(y, x);
			}
		}

		// バッファ画像の更新
		panoramaImg = res.clone();
	}
	
}
