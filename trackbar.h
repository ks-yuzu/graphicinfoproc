#include "opencv.h"

class Trackbar
{
	public:
		Trackbar(const std::string& barName, const std::string windowName, int maxVal, int initVal = 0)
			: name(barName), winName(windowName), value(initVal)
		{
			cv::createTrackbar(name, winName, &value, maxVal);
		}

		// トラックバーの値を返すファンクタ
		int operator()(){ return value; }

	private:
		int value;
		std::string name;
		std::string winName;
};

