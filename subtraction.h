//class Subtraction
//{};

//void UpdateBaseImage(const std::vector<cv::Mat>& now, std::vector<cv::Mat>& last, std::vector<cv::Mat>& bg, const cv::Mat& bgMask)
//{
//	static InputMgr& key = InputMgr::GetInstance();		//singleton
//
//	// 前フレーム画像の更新
//	for (int i = 0; i < 3; i++)
//		last[i] = now[i].clone();
//
//	// 背景画像の更新
//	cv::Mat updateMask = GetAutoBgUpdateMask(bgMask);
//
//	if (key('U') == 1) // uを押下した瞬間（キー押下フレーム数が1のとき）全更新
//		for(int i = 0; i < 3; i++)
//			bg[i] = now[i].clone();
//	else
//		for(int i = 0; i < 3; i++)
//			now[i].copyTo(bg[i], updateMask);
//}
//
//
//cv::Mat GetAutoBgUpdateMask(const cv::Mat& bgMask)
//{
//	const int numFrame = 15; // FPS30を想定 -> 0.2秒継続で背景化
//	static std::list<cv::Mat> bgDiffLog;
//
////	*DispWindow::GetDispWindow(0) << "frame-log : " << bgDiffLog.size() << Command::endline;
//
//	// 背景差分ログの更新
//	if( bgDiffLog.size() == numFrame )	    bgDiffLog.pop_front();
//	else if( bgDiffLog.size() < numFrame )  ;//VOID
//	else                                    logwnd << "too many frame-log" << Command::endline;
//
//	cv::dilate(bgMask, bgMask, cv::Mat(), cv::Point(-1, -1), 3);
//	cv::erode(bgMask, bgMask, cv::Mat(), cv::Point(-1, -1), 3);
//	bgDiffLog.push_back(bgMask.clone());
//
//	// 更新領域の抽出
//	cv::Mat updateMask = bgDiffLog.front().clone();
//	for each (cv::Mat diff in bgDiffLog)
//	{
//		cv::bitwise_and(updateMask, diff, updateMask);
//	}
//
//	return updateMask;
//}
//
//
//std::vector<cv::Mat> MakeDiff(const std::vector<cv::Mat>& img1, const std::vector<cv::Mat>& img2)
//{
//	std::vector<cv::Mat> diff(3);
//	for(int i = 0; i < 3; i++)
//		cv::absdiff(img1[i], img2[i], diff[i]);
//
//	return diff;
//}
//
//std::vector<cv::Mat> ThresholdMatsVector(const std::vector<cv::Mat>& src, const std::vector<int>& thresholds)
//{
//	std::vector<cv::Mat> dst(3);
//
//	for(int i = 0; i < 3; i++)
//		cv::threshold(src[i], dst[i], thresholds[i], 255, CV_THRESH_BINARY);
//
//	return dst;
//}