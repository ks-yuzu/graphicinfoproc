#pragma once

#include "input.h"

#include "opencv.h"
#include "my-mat.h"

#include "debug-window\log_window.h"
#include "debug-window\disp_window.h"


class MovieOut
{
	public:
		// 形式指定用の定数
		static const int NO_COMPRESS = CV_FOURCC_MACRO('D','I','B',' ');
		static const int MP4V        = CV_FOURCC_MACRO('m','p','4','v');

		MovieOut(std::string filename, int width, int height)
		: fRec(false), videoWriter( filename, NO_COMPRESS, 30, cv::Size(width,height) )
		{}

		MovieOut(std::string filename, cv::Size size)
		: fRec(false), videoWriter( filename, NO_COMPRESS, 30, size )
		{}
			
		MovieOut(int width, int height)
		: fRec(false), videoWriter( "NoTitle.avi", NO_COMPRESS, 30, cv::Size(width,height) )
		{}

		MovieOut(cv::Size size)
//		: fRec(false), videoWriter( "NoTitle.avi", NO_COMPRESS, 30, size )
		{}
		
		virtual ~MovieOut() = default;


		void Rec(const MyMat& image)
		{
			static InputMgr& input = InputMgr::GetInstance();

			if( input('V') && input('B') == 1 )
			{
				fRec = true;
				logwnd << "[ record ]  start" << Command::endline;
			}
			if( input('V') && input('E') == 1 )
			{
				fRec = false;
				logwnd << "[ record ]  stop" << Command::endline;
			}

			// videoWriterに書き込み
			if( fRec ) videoWriter << image;

			*DispWindow::GetDispWindow(0) << "Recording ... " << (fRec ? "yes" : "no") << Command::endline;
		}


		bool CheckOpened()
		{
			if( !videoWriter.isOpened() )
				logwnd << "[ video-writer ]  Not Opend" << Command::endline;
			return videoWriter.isOpened();
		}


	private:
		bool fRec;
		cv::VideoWriter videoWriter;

};

