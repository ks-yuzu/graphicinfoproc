#pragma once

#include "opencv.h"

class Camera
{
	public:
		Camera(int num = 0)	: camera( cv::VideoCapture(num) ) {}
		virtual ~Camera() = default;

		// カメラからの入力画像を返すファンクタ
		const cv::Mat& operator()()
		{
			return image;
		}

		bool Update()
		{
			// カメラのチェックをして、確保できていれば映像を取得
			if( camera.isOpened() )
				camera >> image;

			return true;
		}

		bool CheckOpened()
		{
			if( !camera.isOpened() )
				logwnd << "[ camera ]  Not Found Camera" << Command::endline;
			return camera.isOpened();
		}

	private:
		cv::VideoCapture camera;
		cv::Mat image;
};

