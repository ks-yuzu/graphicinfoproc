#include <memory>
#include "opencv.h"
#include "i-mouse.h"

class TemplateMatch
{
	public:
		TemplateMatch(iMouseInput *am) : mouse(am), pTemplateImg(new cv::Mat), detector(1000){}

		void UpdateTemplateImage(cv::Mat& input_copy);
		const cv::Mat& GetTemplateImage() { return *pTemplateImg; };

		void ProcessMatching(const cv::Mat& input, cv::Mat& res);

		bool fUpdated = false;

	private:
	  // マウス情報ののインターフェイス
		iMouseInput *mouse;

	  // テンプレート画像関連
		std::unique_ptr<cv::Mat> pTemplateImg;

		// ラバーバンド関連
		bool fRubberband = false;
		cv::Point rubBase;

	  // SURF関連
		cv::SurfFeatureDetector detector;       // 特徴点抽出インターフェイス
		cv::SurfDescriptorExtractor extractor;  // 特徴記述インターフェイス

		// 特徴点用配列
		std::vector<cv::KeyPoint> keypointsTemplate;
		std::vector<cv::KeyPoint> keypointsInput;

		// featurePointにおける特徴量用配列
		cv::Mat descriptorTemplate;
		cv::Mat descriptorInput;
};

inline void TemplateMatch::UpdateTemplateImage(cv::Mat& input_copy)
{
  // ドラッグの検出処理
	// 左クリックが押された時
	if( mouse->NowDownFlag() )
	{
		fRubberband = true;
		rubBase = mouse->MousePos();
	}
	// 左クリックが離された時
	if( mouse->NowUpFlag() )
	{
		fRubberband = false;

		// テンプレート画像の切り出し。Matのコンストラクタを用いて処理する。
		if(rubBase != mouse->MousePos())
		{
			pTemplateImg.reset( new cv::Mat(input_copy, cv::Rect(rubBase, mouse->MousePos())) );
		}

		fUpdated = true;
	}

	// ラバーバンド（ドラッグ時に表示される四角）の表示
	if( fRubberband ) { cv::rectangle(input_copy, rubBase, mouse->MousePos(), cv::Scalar(0, 0, 255)); }
}

inline void TemplateMatch::ProcessMatching(const cv::Mat& input, cv::Mat& res)
{
	auto min = [](size_t a, size_t b){ return a < b ? a : b;};

	if( pTemplateImg->empty() ) return;

	fUpdated = false;

	// 正規化2値画像の作成
	cv::Mat templateGray;
	cv::Mat inputGray;
	cv::cvtColor(*pTemplateImg, templateGray, CV_BGR2GRAY);
	cv::cvtColor(input, inputGray, CV_BGR2GRAY);
	cv::normalize(templateGray, templateGray, 0, 255, cv::NORM_MINMAX);
	cv::normalize(inputGray, inputGray, 0, 255, cv::NORM_MINMAX);

	// 特徴点の位置とスケール抽出
	detector.detect(templateGray, keypointsTemplate);
	detector.detect(inputGray, keypointsInput);

	// 上記特徴点における特徴ベクトル（輝度勾配ヒストグラム）抽出
	extractor.compute(templateGray, keypointsTemplate, descriptorTemplate);
	extractor.compute(inputGray, keypointsInput, descriptorInput);

	// マッチング処理
	std::vector<cv::DMatch> matchTmpToIn, matchInToTmp;  // DMatch : ディスクリプタのマッチングインターフェイス
	cv::FlannBasedMatcher matcher;
	matcher.match(descriptorTemplate, descriptorInput, matchTmpToIn);
	matcher.match(descriptorInput, descriptorTemplate, matchInToTmp);

	// クロスチェック(ブルートフォースに対する高精度化処理)
	std::vector<cv::DMatch>& match = matchTmpToIn;
	//for(size_t i = 0; i < min(matchTmpToIn.size(), matchInToTmp.size()); ++i)
	//{
	//	const cv::DMatch& fw = matchInToTmp[i];
	//	const cv::DMatch& bw = matchTmpToIn[fw.trainIdx];
	//	if( bw.trainIdx == fw.queryIdx ) { match.push_back(fw); }
	//}

	// keypointの選別
	double minDist = 10000, maxDist = 0;
	for(size_t i = 0; i < match.size(); ++i)
	{
		double dist = match[i].distance;
		minDist = minDist < dist ? minDist : dist;
		maxDist = maxDist > dist ? maxDist : dist;
	}

	std::vector<cv::DMatch> selectedMatch;
	for(size_t i = 0; i < match.size(); ++i)
	{
		if(match[i].distance < 5 * minDist)
		{
			selectedMatch.push_back(match[i]);
		}
	}

	// 対応線を50本まで表示
	size_t numLine = min(50, selectedMatch.size());
	std::nth_element(std::begin(selectedMatch), std::begin(selectedMatch) + numLine-1, std::end(selectedMatch));
	selectedMatch.erase(std::begin(selectedMatch)+numLine, std::end(selectedMatch));

	// 出力画像の作成
	cv::drawMatches(*pTemplateImg, keypointsTemplate, input, keypointsInput, selectedMatch, res,
		cv::Scalar_<double>::all(-1), cv::Scalar_<double>::all(-1), std::vector<char>(), 2 | 4);


	if( selectedMatch.size() > 10 )
	{

		// 対象物体領域の特定
		std::vector<cv::Point2f> tmpVec, inVec;
		for( auto dmatch : selectedMatch )
		{
			tmpVec.push_back( keypointsTemplate[ dmatch.queryIdx ].pt );
			inVec.push_back( keypointsInput[ dmatch.trainIdx ].pt );
		}

		// 変換行列の作成
		cv::Mat translateMat = cv::findHomography(tmpVec, inVec, cv::RANSAC);

		std::vector<cv::Point2f> cornersTmp =
		{
			cv::Point(0,0),
			cv::Point(pTemplateImg->cols, 0),
			cv::Point(pTemplateImg->cols, pTemplateImg->rows),
			cv::Point(0, pTemplateImg->rows),
		};

		std::vector<cv::Point2f> cornersInput;

		cv::perspectiveTransform(cornersTmp, cornersInput, translateMat);

		// 対象領域の描画
		auto drawColor = cv::Scalar(0, 255, 255);
		auto base = cv::Point2f((float)pTemplateImg->cols, 0);
		cv::line( res, base + cornersInput[0], base + cornersInput[1], drawColor, 4);
		cv::line( res, base + cornersInput[1], base + cornersInput[2], drawColor, 4);
		cv::line( res, base + cornersInput[2], base + cornersInput[3], drawColor, 4);
		cv::line( res, base + cornersInput[3], base + cornersInput[0], drawColor, 4);
	}

}




